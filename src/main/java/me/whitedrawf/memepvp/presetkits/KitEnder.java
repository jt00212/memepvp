package me.whitedrawf.memepvp.presetkits;

import me.whitedrawf.memepvp.Main;
import org.bukkit.*;
import org.bukkit.Color;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

/** The ender kit class.
 * @version 1.0
 * @author WhiteDwarf
 */
public class KitEnder implements Listener {

    private static Inventory inventory = Bukkit.createInventory(null, InventoryType.PLAYER);

    public static ArrayList<String> playersWithKit = new ArrayList<String>();

    private static HashMap<UUID, Long> cooldown = new HashMap<UUID, Long>();

    /** Initializes the inventory of the kit.
     *
     * @precondition: none
     * @postcondition: none
     */
    public KitEnder() {
        this.buildInventory();
    }

    /** Adds the player to the list and sets their inventory to kits.
     *
     * @precondition: !inventory.isEmpty()
     * @postcondition: none
     *
     * @param p - the player being set the kit.
     */
    public static void addPlayer(Player p) {
        if (inventory.isEmpty()) {
            Main.mainclass.printConsoleMessage(Main.mainclass.title + " Class was never initialized.");
        }
        playersWithKit.add(p.getName());
        Main.mainclass.playersWithKit.add(p.getUniqueId());
        p.getInventory().setContents(inventory.getContents());
    }

    /** Removes a player from the list and resets their inventory.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @param p - the player being removed from the kit.
     */
    public static void removePlayer(Player p) {
        playersWithKit.remove(p.getName());
        Main.mainclass.playersWithKit.remove(p.getUniqueId());
        p.getInventory().clear();
    }

    /** Builds the inventory.
     *
     * @precondition: none
     * @postcondition: none
     */
    public void buildInventory() {
        inventory.clear();

        /* Weapons */
        ItemStack mainSword = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta mainSwordMeta = mainSword.getItemMeta();
        mainSwordMeta.setDisplayName(ChatColor.DARK_PURPLE + "Ender's Blade");
        mainSwordMeta.setLore(Arrays.asList("Where did it come from???", "Where did it go???", "Where did it come from?", "Cotton Eye Joe"));
        mainSword.setItemMeta(mainSwordMeta);
        mainSword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 9);

        inventory.setItem(0, mainSword);

        /* Food / Healing Items */
        inventory.setItem(1, new ItemStack(Material.ENCHANTED_GOLDEN_APPLE, 3));
        inventory.setItem(2, new ItemStack(Material.GOLDEN_APPLE, 12));
        inventory.setItem(3, new ItemStack(Material.COOKED_BEEF, 64));
        ItemStack healthPotion = new ItemStack(Material.SPLASH_POTION);
        PotionMeta healthMeta = (PotionMeta) healthPotion.getItemMeta();
        healthMeta.setColor(Color.PURPLE);
        healthMeta.setDisplayName(ChatColor.DARK_PURPLE + "Health Potion");
        healthMeta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 5, 1), true);
        healthPotion.setItemMeta(healthMeta);
        for (int index = 4; index < 36; index++) {
            inventory.setItem(index, healthPotion);
        }

        /* armor */
        ItemStack helmet = new ItemStack(Material.DIAMOND_HELMET);
        helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        ItemStack pants = new ItemStack(Material.IRON_LEGGINGS);
        pants.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS);
        boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);

        inventory.setItem(39, helmet);
        inventory.setItem(38, chestplate);
        inventory.setItem(37, pants);
        inventory.setItem(36, boots);
    }

    /** Listens for events pertaining to the kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @EventHandler: InventoryClickEvent
     */
    @EventHandler
    public void onInventoryClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (playersWithKit.contains(p.getName())) {
            if (p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_SWORD
                    && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
                Material targetBlock = p.getTargetBlockExact(50).getType();
                if (targetBlock.isSolid()) {
                    if (cooldown.containsKey(p.getUniqueId()) && cooldown.get(p.getUniqueId()) > System.currentTimeMillis()
                            && !Main.mainclass.playersIgnoreCooldown.contains(p.getName())) {
                        long remainingTime = cooldown.get(p.getUniqueId()) - System.currentTimeMillis();
                        p.sendMessage(ChatColor.RED + "You are still on cooldown for " + String.valueOf(remainingTime/1000) + " seconds.");
                    } else {
                        Location newLocation = p.getTargetBlockExact(50).getLocation();
                        p.spawnParticle(Particle.CLOUD, p.getLocation(), 15);
                        p.teleport(new Location(newLocation.getWorld(), newLocation.getX(), newLocation.getY() + 1, newLocation.getZ(),
                                p.getLocation().getYaw(), p.getLocation().getPitch()));
                        p.spawnParticle(Particle.CLOUD, p.getLocation(), 15);
                        p.playSound(p.getLocation(), Sound.ENTITY_ENDERMAN_TELEPORT, 2.0F, 1.0F);
                        cooldown.put(p.getUniqueId(), System.currentTimeMillis() + (15 * 1000));
                    }
                }
            }
        }
    }
}
