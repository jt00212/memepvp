package me.whitedrawf.memepvp.presetkits;

import me.whitedrawf.memepvp.Main;
import me.whitedrawf.memepvp.model.custom.abilities.BombShooter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/** The bomber kit class.
 * @version 1.0
 * @author WhiteDwarf
 */
public class KitBomber {

    private static Inventory inventory = Bukkit.createInventory(null, InventoryType.PLAYER);

    public static ArrayList<String> playersWithKit = new ArrayList<String>();


    /* Builds the inventory for the class */
    public KitBomber() {
        this.buildInventory();
    }

    public static void addPlayer(Player p) {
        if (inventory.isEmpty()) {
            Main.mainclass.printConsoleMessage(Main.mainclass.title + " Class was never initialized.");
        }
        playersWithKit.add(p.getName());
        Main.mainclass.playersWithKit.add(p.getUniqueId());
        activateAbilities(p);
        p.getInventory().setContents(inventory.getContents());
    }

    private static void activateAbilities(Player p) {
        BombShooter.add(p, Material.GOLDEN_SWORD, 4);
    }

    /** Removes a player from the list and resets their inventory.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @param p - the player being removed from the kit.
     */
    public static void removePlayer(Player p) {
        playersWithKit.remove(p.getName());
        Main.mainclass.playersWithKit.remove(p.getUniqueId());
        deactivateAbilities(p);
        p.getInventory().clear();
    }

    private static void deactivateAbilities(Player p) {
        BombShooter.remove(p);
    }

    private void buildInventory() {
        inventory.clear();

        ItemStack dagger = new ItemStack(Material.GOLDEN_SWORD);
        dagger.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 15);
        ItemMeta daggerMeta = dagger.getItemMeta();
        daggerMeta.setDisplayName("A sneaky boy");
        dagger.setItemMeta(daggerMeta);

        inventory.setItem(0, dagger);
    }
}
