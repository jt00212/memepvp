package me.whitedrawf.memepvp.presetkits;

import me.whitedrawf.memepvp.Main;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/** The ninja kit class.
 * @version 1.0
 * @author WhiteDwarf
 */
public class KitNinja implements Listener {

    private static Inventory inventory = Bukkit.createInventory(null, InventoryType.PLAYER);
    private static HashMap<UUID, Long> cooldown = new HashMap<UUID, Long>();

    public static ArrayList<String> playersWithKit = new ArrayList<String>();


    /** Builds the inventory for the class.
     *
     */
    public KitNinja() {
        this.buildInventory();
    }

    public static void addPlayer(Player p) {
        if (inventory.isEmpty()) {
            Main.mainclass.printConsoleMessage(Main.mainclass.title + " Class was never initialized.");
        }
        playersWithKit.add(p.getName());
        Main.mainclass.playersWithKit.add(p.getUniqueId());
        p.getInventory().setContents(inventory.getContents());
    }

    /** Removes a player from the list and resets their inventory.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @param p - the player being removed from the kit.
     */
    public static void removePlayer(Player p) {
        playersWithKit.remove(p.getName());
        Main.mainclass.playersWithKit.remove(p.getUniqueId());
        p.getInventory().clear();
    }

    private void buildInventory() {
        inventory.clear();

        ItemStack dagger = new ItemStack(Material.IRON_SWORD);
        dagger.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 15);
        ItemMeta daggerMeta = dagger.getItemMeta();
        daggerMeta.setDisplayName("A sneaky boy");
        dagger.setItemMeta(daggerMeta);

        inventory.setItem(0, dagger);
    }

    /** Starts the timer for invisibility
     *
     * @precondition: none
     * @postcondition: none
     */
    private void startInvisibility(Player p) {
        p.setInvisible(true);
        p.sendMessage("Sneaky sneaky");
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.scheduleSyncDelayedTask(Main.mainclass, new Runnable() {
            @Override
            public void run() {
                p.setInvisible(false);
                p.sendMessage("uh-oh speggettios.");
            }
        }, 100);
    }

    /** Listens for events for the kits.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @EventHandler: PlayerInteractEvent
     */
    @EventHandler
    public void onInventoryClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (playersWithKit.contains(p.getName())) {
            if (p.getInventory().getItemInMainHand().getType() == Material.IRON_SWORD
                    && (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
                if (cooldown.containsKey(p.getUniqueId()) && cooldown.get(p.getUniqueId()) > System.currentTimeMillis()
                        && !Main.mainclass.playersIgnoreCooldown.contains(p.getName())) {
                    long remainingTime = cooldown.get(p.getUniqueId()) - System.currentTimeMillis();
                    p.sendMessage(ChatColor.RED + "You are still on cooldown for " + String.valueOf(remainingTime/1000) + " seconds.");
                } else {
                    p.playSound(p.getLocation(), Sound.ENTITY_ENDER_DRAGON_FLAP, 2.0F, 1.0F);
                    startInvisibility(p);
                    cooldown.put(p.getUniqueId(), System.currentTimeMillis() + (30 * 1000));
                }
            }
        }
    }
}
