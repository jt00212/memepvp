package me.whitedrawf.memepvp;

import com.google.common.primitives.Ints;
import me.whitedrawf.memepvp.commands.KitAccess;
import me.whitedrawf.memepvp.commands.PlayerCommands;
import me.whitedrawf.memepvp.globalrules.BasicRules;
import me.whitedrawf.memepvp.model.custom.abilities.BombShooter;
import me.whitedrawf.memepvp.model.custom.abilities.Martyrdom;
import me.whitedrawf.memepvp.presetkits.KitBomber;
import me.whitedrawf.memepvp.presetkits.KitEnder;
import me.whitedrawf.memepvp.presetkits.KitNinja;
import me.whitedrawf.memepvp.ui.GUI;
import me.whitedrawf.memepvp.ui.GUIListener;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.UUID;

public final class Main extends JavaPlugin implements Listener {

    public static Main mainclass;
    public String title;
    public ArrayList<String> playersIgnoreCooldown;
    public ArrayList<UUID> playersWithKit;

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.registerEvents();
        this.registerCommands();
        this.buildKits();

        this.playersIgnoreCooldown = new ArrayList<String>();
        this.playersWithKit = new ArrayList<UUID>();

        mainclass = this;
        title = "";
    }

    /* Builds the inventories of the kits. */
    private void buildKits() {
        new KitEnder();
        new KitNinja();
        new KitBomber();
    }

    private void registerEvents() {
        PluginManager pm = getServer().getPluginManager();

        //Kits
        pm.registerEvents(new KitEnder(), this);
        pm.registerEvents(new KitNinja(), this);

        //Abilities
        pm.registerEvents(new Martyrdom(), this);
        pm.registerEvents(new BombShooter(), this);

        //Basic Listeners
        pm.registerEvents(new GUIListener(), this);
        pm.registerEvents(new BasicRules(), this);
    }

    private void registerCommands() {
        getCommand("meme").setExecutor(new PlayerCommands());
        getCommand("accesskit").setExecutor(new KitAccess());
        getCommand("test").setExecutor(this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void removeKitsFromPlayer(Player p) {
        if (playersWithKit.contains(p.getUniqueId())) {
            KitEnder.removePlayer(p);
            KitNinja.removePlayer(p);
        }
    }

    public void printConsoleMessage(String message) {
        this.getServer().getConsoleSender().sendMessage(message);
    }
}
