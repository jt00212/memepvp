package me.whitedrawf.memepvp.commands;

import me.whitedrawf.memepvp.Main;
import me.whitedrawf.memepvp.presetkits.KitBomber;
import me.whitedrawf.memepvp.presetkits.KitEnder;
import me.whitedrawf.memepvp.presetkits.KitNinja;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KitAccess implements CommandExecutor {

    /**
     * Executes the given command, returning its success.
     * If false is returned, then the "usage" plugin.yml entry for this command
     * (if defined) will be sent to the player.
     *
     * @param sender  Source of the command
     * @param command Command which was executed
     * @param label   Alias of the command which was used
     * @param args    Passed command arguments
     * @return true if a valid command, otherwise false
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player) {
            Player p = (Player) sender;
            if (args.length == 0) {
                p.sendMessage("Please specify a kit.");
            } else {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("ender")) {
                        KitEnder.addPlayer(p);
                        p.sendMessage("You have been assigned to kit: Ender");
                    } else if (args[0].equalsIgnoreCase("ninja")) {
                        KitNinja.addPlayer(p);
                        p.sendMessage("Your a ninja now harry.");
                    } else if (args[0].equalsIgnoreCase("bomber")) {
                        KitBomber.addPlayer(p);
                        p.sendMessage("oof");
                    }
                }
            }
        } else {
            Main.mainclass.printConsoleMessage("This command is only for players.");
        }

        return true;
    }
}
