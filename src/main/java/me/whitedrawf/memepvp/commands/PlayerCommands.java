package me.whitedrawf.memepvp.commands;

import me.whitedrawf.memepvp.Main;
import me.whitedrawf.memepvp.ui.GUI;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerCommands implements CommandExecutor {

    /**
     * Executes the given command, returning its success.
     * If false is returned, then the "usage" plugin.yml entry for this command
     * (if defined) will be sent to the player.
     *
     * @param sender  Source of the command
     * @param command Command which was executed
     * @param label   Alias of the command which was used
     * @param args    Passed command arguments
     * @return true if a valid command, otherwise false
     */
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            if (args.length == 0) {
                GUI gui = new GUI();
                gui.openMain(p);
            } else {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("quit")) {
                        Main.mainclass.removeKitsFromPlayer(p);
                    } else if (args[0].equalsIgnoreCase("ignorecooldown")) {
                        if (Main.mainclass.playersIgnoreCooldown.contains(p.getName())) {
                            Main.mainclass.playersIgnoreCooldown.remove(p.getName());
                        } else {
                            Main.mainclass.playersIgnoreCooldown.add(p.getName());
                        }
                    }
                }
            }
        } else {
            Main.mainclass.printConsoleMessage("Well hello there.");
        }
        return true;
    }
}
