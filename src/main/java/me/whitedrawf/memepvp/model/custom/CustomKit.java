package me.whitedrawf.memepvp.model.custom;

import org.bukkit.inventory.Inventory;

import java.util.ArrayList;

public class CustomKit {

    private String name;
    private String type;
    private String permission;
    private int cost;
    private AbilityManager abilities;
    private Inventory inventory;


    /** Creates a kit with a specified name, type, list of abilities, and cost.
     *  Also instatiates the permission, (if null means no permission)
     *
     * @precondition: name != null && !name.isEmpty()
     *                && type != null && !type.isEmpty()
     *                && abilities != null && cost >= 0
     *                && inventory != null
     * @postcondition: getName() == name && type == getType()
     *                 && getAbilities() == abilities
     *                 && getCost() == cost &&
     *
     * @param name - the name of the kit.
     * @param type - the style/type of the class.
     * @param abilities - the list of abilities the kit has.
     * @param cost - the cost of the class.
     * @param inventory - the items for the kit.
     * @param permission - the permissions required for the kit.
     */
    public CustomKit(String name, String type, AbilityManager abilities, int cost, Inventory inventory, String permission) {
        this.setName(name);
        this.setType(type);
        this.setAbilityManager(abilities);
        this.setCost(cost);
        this.setInventory(inventory);
        this.setPermission(permission);
    }

    /** Sets a new name for the kit.
     *
     * @precondition: name != null && !name.isEmpty()
     * @postcondition: getName() == name
     *
     * @param name - the name of the kit.
     */
    public void setName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("The name must be valid.");
        }
        this.name = name;
    }

    /** Gets the name of the kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return (String) the name of the kit.
     */
    public String getName() {
        return this.name;
    }

    /** Sets the type of the class.
     *
     * @precondition: type != null && !type.isEmpty()
     * @postcondition: getType() == type
     *
     * @param type - the kits type
     */
    public void setType(String type) {
        if (type == null || type.isEmpty()) {
            throw new IllegalArgumentException("Invalid Type.");
        }
        if (type.equalsIgnoreCase("archer") || type.equalsIgnoreCase("Warrior")
            || type.equalsIgnoreCase("Close-Combat") || type.equalsIgnoreCase("Mage")
            || type.equalsIgnoreCase("Wizard")) {
            this.type = type;
        } else {
            throw new IllegalArgumentException("Type is not apart of the list, please check spelling.");
        }
    }

    /** Returns the type of the kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return (String) the type of the kit
     */
    public String getType() {
        return this.type;
    }

    /** Sets the abilities for the kit.
     *
     * @precondition: abilities != null
     * @postcondition: getAbilities() == abilities
     *
     * @param abilities - the list of abilities
     */
    public void setAbilityManager(AbilityManager abilities) {
        if (abilities == null) {
            throw new IllegalArgumentException("The abilities cannot be null.");
        }
        this.abilities = abilities;
    }

    /** Gets the ability manager for the kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return (AbilityManager) the ability manager for the kit.
     */
    public AbilityManager getAbilityManager() {
        return this.abilities;
    }

    /** Sets the cost for the kit.
     *
     * @precondition: cost >= 0.0
     * @postcondition: none
     *
     * @param cost - the cost of the kit.
     */
    public void setCost(int cost) {
        if (cost < 0) {
            throw new IllegalArgumentException("The cost cannot be negative.");
        }
        this.cost = cost;
    }

    /** Gets the cost for the kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return the cost of the kit.
     */
    public int getCost() {
        return this.cost;
    }

    /** Sets the inventory for the kit.
     *
     * @precondition: inventory != null
     * @postcondition: getInventory() == inventory
     */
    public void setInventory(Inventory inventory) {
        if (inventory == null) {
            throw new IllegalArgumentException("The inventory is invalid.");
        }
        this.inventory = inventory;
    }

    /** Returns the inventory for the kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return (Inventory) the inventory for the kit.
     */
    public Inventory getInventory() {
        return this.inventory;
    }

    /** Sets the permission for the kit.
     *
     * @precondition: none
     * @postcondition: none
     */
    public void setPermission(String permission) {
        if (permission == null) {
            this.permission = "";
        } else {
            this.permission = permission;
        }
    }

    /** Gets the permission (if applicable) for the kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return the permission for the kit.
     */
    public String getPermission() {
        return this.permission;
    }
}
