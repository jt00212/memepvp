package me.whitedrawf.memepvp.model.custom;

import me.whitedrawf.memepvp.model.custom.abilities.Martyrdom;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class AbilityManager {

    private ArrayList<Ability> abilities;

    public AbilityManager() {
        this.abilities = new ArrayList<Ability>();
    }

    /** Adds a new ability to the list. (default ability settings)
     *
     * @precondition: abilityName != null && !abilityNames.contains(abilityName)
     * @postcondition: abilityNames.size() == abilityNames.size()@pre + 1
     *
     * @param abilityName - the name of the new ability
     */
    public void add(String abilityName) {
        if (abilityName == null) {
            throw new IllegalArgumentException("The ability is not valid.");
        }
        for (Ability currentAbility : this.abilities) {
            if (currentAbility.getType().equalsIgnoreCase(abilityName)) {
                throw new IllegalArgumentException("The ability is already on the list.");
            }
        }
        if (!this.isAbility(abilityName.toLowerCase())) {
            throw new IllegalArgumentException("The ability is not an ability, check the spelling.");
        }
        this.abilities.add(new Ability(abilityName.toLowerCase(), 1, null));
    }

    /** Adds a new ability to the list. (Two param constructor with specified level settings, no args.)
     *
     * @precondition: abilityName != null && !abilityNames.contains(abilityName)
     *                && level > 0
     * @postcondition: abilityNames.size() == abilityNames.size()@pre + 1
     *
     * @param abilityName - the name of the new ability
     * @param level - the level of the new abilit
     *
     */
    public void add(String abilityName, int level) {
        if (abilityName == null) {
            throw new IllegalArgumentException("The ability is not valid.");
        }
        if (level <= 0) {
            throw new IllegalArgumentException("The level must be atleast 1.");
        }
        for (Ability currentAbility : this.abilities) {
            if (currentAbility.getType().equalsIgnoreCase(abilityName)) {
                throw new IllegalArgumentException("The ability is already on the list.");
            }
        }
        if (!this.isAbility(abilityName.toLowerCase())) {
            throw new IllegalArgumentException("The ability is not an ability, check the spelling.");
        }
        this.abilities.add(new Ability(abilityName.toLowerCase(), level, null));
    }

    /** Adds a new ability to the list. (Two param constructor with specified level settings, no args.)
     *
     * @precondition: abilityName != null && !abilityNames.contains(abilityName)
     *                && level > 0 && args != null
     * @postcondition: abilityNames.size() == abilityNames.size()@pre + 1
     *
     * @param abilityName - the name of the new ability
     * @param level - the level of the new ability
     * @param args - (optional) arguments
     *
     */
    public void add(String abilityName, int level, String args) {
        if (abilityName == null) {
            throw new IllegalArgumentException("The ability is not valid.");
        }
        if (level <= 0) {
            throw new IllegalArgumentException("The level must be atleast 1.");
        }
        if (args == null) {
            throw new IllegalArgumentException("Invalid Arguments.");
        }
        for (Ability currentAbility : this.abilities) {
            if (currentAbility.getType().equalsIgnoreCase(abilityName)) {
                throw new IllegalArgumentException("The ability is already on the list.");
            }
        }
        if (!this.isAbility(abilityName.toLowerCase())) {
            throw new IllegalArgumentException("The ability is not an ability, check the spelling.");
        }
        this.abilities.add(new Ability(abilityName.toLowerCase(), level, args));
    }

    /** Removes a ability from the list.
     *
     * @precondition: abilityName != null && !abilityName.isEmpty()
     *                && abilityNames.contains(abilityName)
     * @postcondition: abilityNames.size() == abilityNames.size()@pre - 1
     *
     * @param abilityName - the ability being removed. (added by anna - poo)
     */
    public void remove(String abilityName, UUID uuid) {
        if (abilityName == null) {
            throw new IllegalArgumentException("The ability is not valid.");
        }
        for (Ability currentAbility : this.abilities) {
            if (!currentAbility.getType().equalsIgnoreCase(abilityName)) {
                throw new IllegalArgumentException("The ability is not on the list.");
            }
        }
        if (!this.isAbility(abilityName.toLowerCase())) {
            throw new IllegalArgumentException("The ability is not an ability, check the spelling.");
        }
        Ability theAbility = null;
        for (Ability currentAbility : this.abilities) {
            if (currentAbility.getType().equalsIgnoreCase(abilityName)) {
                theAbility = currentAbility;
            }
        }
        this.removeAllAbilities(Bukkit.getPlayer(uuid));
        this.abilities.remove(theAbility);
    }

    /** Adds all the abilities to the player
     *
     * @precondition: p != null
     * @postcondition: none
     *
     * @param p - the player it is being added to.
     */
    public void addAbilities(Player p) {
        for (Ability currentAbility : this.abilities) {
            String type = currentAbility.getType();
            switch (type) {
                case "martyrdom":
                    Martyrdom.playersWithAbility.add(p.getName());
                    Martyrdom.abilityLevel.put(p.getName(), currentAbility.getLevel());
                    break;
                case "super-jump":
                    break;
            }
        }
    }

    /** Clears the player of all abilities.
     *
     * @precondition: p != null
     * @postcondition: all abilities removed.
     */
    public void removeAllAbilities(Player p) {
        Martyrdom.playersWithAbility.remove(p.getName());
        Martyrdom.abilityLevel.remove(p.getName());
    }

    /** Returns the list of abilities.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return (ArrayList<Ability>) A list of abilities.
     */
    public ArrayList<Ability> getAbilities() {
        return this.getAbilities();
    }

    private boolean isAbility(String ability) {
        String[] listOfAbilities = {"martyrdom"};
        for (String currentAbility : listOfAbilities) {
            if (currentAbility.equalsIgnoreCase(ability)) {
                return true;
            }
        }
        return false;
    }

}
