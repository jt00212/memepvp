package me.whitedrawf.memepvp.model.custom.abilities;

import me.whitedrawf.memepvp.Main;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.HashMap;
import java.util.UUID;

public class BombShooter implements Listener {

    private static HashMap<UUID, Long> cooldown = new HashMap<UUID, Long>();
    private static HashMap<UUID, Long> cooldownTime = new HashMap<UUID, Long>();

    public static HashMap<UUID, Material> use = new HashMap<UUID, Material>();

    /** Adds a player to the ability with a specified weapon and cooldown time in seconds.
     *
     * @param p - The player with the ability.
     * @param weapon - The material (weapon) the ability is tied to.
     * @param cooldown - time (in seconds) inbetween uses.
     */
    public static void add(Player p, Material weapon, long cooldown) {
        use.put(p.getUniqueId(), weapon);
        cooldownTime.put(p.getUniqueId(), cooldown);
    }

    public static void remove(Player p) {
        use.remove(p.getUniqueId());
        cooldownTime.remove(p.getUniqueId());
    }

    private void throwBomb(Player p) {
        Entity tnt = p.getWorld().spawn(p.getLocation(), TNTPrimed.class);
        ((TNTPrimed)tnt).setFuseTicks(50);
        tnt.setVelocity(p.getLocation().getDirection().multiply(2D));
    }

    @EventHandler
    public void throwTNT(PlayerInteractEvent e) {
        if (use.containsKey(e.getPlayer().getUniqueId())) {
            if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Player p = e.getPlayer();
                if (use.get(p.getUniqueId()) == p.getInventory().getItemInMainHand().getType()) {
                    if (cooldown.containsKey(p.getUniqueId()) && cooldown.get(p.getUniqueId()) > System.currentTimeMillis()
                            && !Main.mainclass.playersIgnoreCooldown.contains(p.getName())) {
                        long remainingTime = cooldown.get(p.getUniqueId()) - System.currentTimeMillis();
                        p.sendMessage(ChatColor.AQUA + "You are still on cooldown for " + String.valueOf(remainingTime / 1000) + " seconds.");
                    } else {
                        throwBomb(p);
                        cooldown.put(p.getUniqueId(), System.currentTimeMillis() + (cooldownTime.get(p.getUniqueId()) * 1000));
                    }
                }
            }
        }
    }

}
