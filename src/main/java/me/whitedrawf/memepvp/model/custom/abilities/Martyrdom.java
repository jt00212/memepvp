package me.whitedrawf.memepvp.model.custom.abilities;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;
import java.util.HashMap;

public class Martyrdom implements Listener {

    public static ArrayList<String> playersWithAbility = new ArrayList<>();
    public static HashMap<String, Integer> abilityLevel = new HashMap<String, Integer>();

    /** Clears all the players from the list.
     *
     * @precondition: none
     * @postcondition: none
     */
    public static void clearPlayers() {
        playersWithAbility.clear();
    }

    /** Checks when a player dies and if they are on the list.
     *  If they are not they are ignored, if they are, it gets
     *  their location and spawns a tnt at the death location.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @param e - the event variable.
     */
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        if (playersWithAbility.contains(e.getEntity().getName())) {
            Player p = (Player) e.getEntity();
            Location deathLocation = e.getEntity().getLocation();
            p.getWorld().spawn(deathLocation, TNTPrimed.class);
        }
    }
}
