package me.whitedrawf.memepvp.model.custom;

/** Creates an ability object.
 * @version 1.0
 * @author WhiteDwarf
 */
public class Ability {

    private String type;
    private int level;
    private String args;

    /** Creates an ability object with a specified type, level, and (optional) arguments.
     *
     * @precondition: type != null && !type.isEmpty() && level >= 1
     * @postcondition: getType() == type && getLevel == level
     *
     * @param type - the type of kit it is.
     * @param level - the level of the ability.
     * @param args - (optional) modifying arguments.
     */
    public Ability(String type, int level, String args) {
        this.setType(type);
        this.setLevel(level);
        this.setArguments(args);
    }

    /** Sets the type of kit it is.
     *
     * @precondition: type != null && !type.isEmpty()
     * @postcondition: getType() == type
     *
     * @param type - the type of kit it is.
     */
    public void setType(String type) {
        if (type == null || type.isEmpty()) {
            throw new IllegalArgumentException("The type is not valid.");
        }
        this.type = type;
    }

    /** Returns the type of kit.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return the type of kit it is.
     */
    public String getType() {
        return this.type;
    }

    /** Sets the level of the ability
     *
     * @precondition: level > 0
     * @postcondition: none
     *
     * @param level - the level of the ability.
     */
    public void setLevel(int level) {
        if (level <= 0) {
           throw new IllegalArgumentException("The level cannot be below 1.");
        }
        this.level = level;
    }

    /** Gets the level of the ability.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return the level of the ability.
     */
    public int getLevel() {
        return this.level;
    }

    /** (Optional) Sets the arguments for the ability.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @param args - the optional arguments.
     */
    public void setArguments(String args) {
        if (args == null) {
            this.args = "";
        } else {
            this.args = args;
        }
    }

    /** Gets the arguments for the ability.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return the arguments for the ability.
     */
    public String getArguments() {
        return this.args;
    }

    /** Is used when converting object data to a string.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @return a formatted string for saving the data.
     */
    public String toString() {
        if (args.isEmpty()) {
            return this.type + ", " + this.level;
        }
        return this.type + ", " + this.level + ", " + this.args;
    }
}
