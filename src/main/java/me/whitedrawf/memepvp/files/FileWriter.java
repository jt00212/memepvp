package me.whitedrawf.memepvp.files;

import me.whitedrawf.memepvp.Main;
import me.whitedrawf.memepvp.model.custom.Ability;
import me.whitedrawf.memepvp.model.custom.AbilityManager;
import me.whitedrawf.memepvp.model.custom.CustomKit;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/** Writes to files.
 * @version 1.0
 * @author WhiteDwarf
 */
public class FileWriter {

    private final File outFile;

    /** Creates a FileWriter class that can write to specified file.
     *
     * @precondition: File != null
     * @postcondition getFile == outfile
     *
     * @param outFile - the file being written to.
     */
    public FileWriter(File outFile) {
        if (outFile == null) {
            throw new IllegalArgumentException("The file cannot be null.");
        }
        this.outFile = outFile;
        this.createFile();
    }

    /** Checks to see if the file was exists already,
     *  if not the file is created.
     *
     * @precondition: none
     * @postcondition: none
     */
    private void createFile() {
        if (!this.exists()) {
            try {
                this.outFile.mkdirs();
                outFile.createNewFile();
                Main.mainclass.printConsoleMessage(Main.mainclass.title + " Creating file " + this.outFile.getName());
            } catch (IOException e) {
                Main.mainclass.printConsoleMessage(Main.mainclass.title + " Error creating file " + this.outFile.getName()
                        + ". Stack trace: " + Arrays.toString(e.getStackTrace()));
            }
        }
    }

    /** Clears the file contents.
     *
     * @precondition: none
     * @postcondition: none
     */
    public void clearFile() {
        try (PrintWriter writer = new PrintWriter(outFile)) {
            writer.write("");
        } catch (Exception e) {
            Main.mainclass.printConsoleMessage("Error clearing file " + this.outFile.getName() + ".");
            this.createFile();
        }
    }

    /** Saves a kit to the respected file.
     *
     * @precondition: Kit != null
     * @postcondition: none
     */
    public void saveCustomKit(CustomKit kit) {
        try (PrintWriter printWriter = new PrintWriter(outFile)) {
            String finalMessage = "";
            finalMessage += "Name: " + kit.getName() + System.lineSeparator();
            finalMessage += "Type: " + kit.getType() + System.lineSeparator();
            finalMessage += "Abilities:" + System.lineSeparator();
            for (Ability ability : kit.getAbilityManager().getAbilities()) {
                finalMessage += ability.toString() + System.lineSeparator();
            }
            finalMessage += "Inventory: " + System.lineSeparator();
            int counter = 0;
            for (ItemStack currentItem : kit.getInventory().getContents()) {
                finalMessage += "Item " + counter + ": " + currentItem.getType();
                if (currentItem.getItemMeta().hasDisplayName()) {
                    finalMessage += "," + currentItem.getItemMeta().getDisplayName();
                }
                if (currentItem.getItemMeta().hasLore()) {
                    finalMessage += "," + currentItem.getItemMeta().getLore();
                }
                if (currentItem.getItemMeta().hasEnchants()) {
                    finalMessage += "," + currentItem.getItemMeta().getEnchants();
                }
                finalMessage += System.lineSeparator();
            }
            finalMessage += "Cost: " + kit.getCost() + System.lineSeparator();
            finalMessage += "Permission: " + kit.getPermission();
        } catch (Exception e) {
            Main.mainclass.printConsoleMessage(Main.mainclass.title + " The file was not found, creating new file.");
            this.createFile();
            this.saveCustomKit(kit);
        }
    }

    private boolean exists() {
        return this.outFile.exists();
    }
}
