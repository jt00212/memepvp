package me.whitedrawf.memepvp.globalrules;

import me.whitedrawf.memepvp.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class BasicRules implements Listener {

    @EventHandler
    public void onItemPickup(EntityPickupItemEvent e) {
        if (e.getEntity() instanceof Player) {
            if (Main.mainclass.playersWithKit.contains(e.getEntity().getUniqueId())) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent e) {
        if (Main.mainclass.playersWithKit.contains(e.getPlayer().getUniqueId())) {
            Main.mainclass.removeKitsFromPlayer(e.getPlayer());
        }
    }

}
