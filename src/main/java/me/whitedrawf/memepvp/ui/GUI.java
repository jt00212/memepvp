package me.whitedrawf.memepvp.ui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;

/** The gui class.
 * @version 1.0
 * @author WhiteDwarf
 */
public class GUI {

    public static ArrayList<String> playersInMenu = new ArrayList<String>();

    private Inventory mainMenu = Bukkit.createInventory(null, InventoryType.CHEST, ChatColor.AQUA + "MemePvP");

    /** Builds the menus
     *
     * @precondition: none
     * @postcondition: none
     */
    public GUI() {
        this.buildMainMenu();
    }

    private void buildMainMenu() {
        ItemStack ender = new ItemStack(Material.ENDER_PEARL);
        ItemMeta enderMeta = ender.getItemMeta();
        enderMeta.setDisplayName(ChatColor.DARK_PURPLE + "Ender");
        ender.setItemMeta(enderMeta);

        ItemStack ninja = new ItemStack(Material.IRON_SWORD);
        ItemMeta ninjaMeta = ninja.getItemMeta();
        ninjaMeta.setDisplayName("Your a wiz.. I mean ninja now harry.");
        ninjaMeta.setLore(Arrays.asList("- Griffin Doors."));
        ninja.setItemMeta(ninjaMeta);

        ItemStack ignoreCooldown = new ItemStack(Material.REDSTONE_TORCH);
        ItemMeta ignoreCooldownMeta = ignoreCooldown.getItemMeta();
        ignoreCooldownMeta.setDisplayName(ChatColor.DARK_RED + "Toggles Ignoring Cooldown");
        ignoreCooldown.setItemMeta(ignoreCooldownMeta);

        ItemStack quit = new ItemStack(Material.BARRIER);
        ItemMeta quitMeta = quit.getItemMeta();
        quitMeta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Quit");
        quit.setItemMeta(quitMeta);

        mainMenu.setItem(0, ender);
        mainMenu.setItem(1, ninja);
        mainMenu.setItem(mainMenu.getSize() - 2, ignoreCooldown);
        mainMenu.setItem(mainMenu.getSize() - 1, quit);
    }

    /** Opens the Main Menu GUI for the player.
     *
     * @precondition: none
     * @postcondition: none
     *
     * @param p - the player who is opening the gui.
     */
    public void openMain(Player p) {
        p.openInventory(mainMenu);
    }
}
