package me.whitedrawf.memepvp.ui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class GUIListener implements Listener {

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (GUI.playersInMenu.contains(e.getPlayer().toString())) {
            GUI.playersInMenu.remove(e.getPlayer().toString());
            e.getPlayer().sendMessage("You're off the list.");
        }
    }

    @EventHandler
    public void onInventoryInteract(InventoryClickEvent e) {
        if (e.getWhoClicked().getOpenInventory().getTitle().equalsIgnoreCase(ChatColor.AQUA + "MemePvP")) {
            e.setCancelled(true);
            Player p = (Player) e.getWhoClicked();
            if (e.getCurrentItem().getType().equals(Material.ENDER_PEARL)) {
                p.performCommand("accesskit ender");
                p.closeInventory();
            }
            if (e.getCurrentItem().getType().equals(Material.IRON_SWORD)) {
                p.performCommand("accesskit ninja");
                p.closeInventory();
            }
            if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
                p.performCommand("meme quit");
                p.closeInventory();
            }
            if (e.getCurrentItem().getType().equals(Material.REDSTONE_TORCH)) {
                p.performCommand("meme ignorecooldown");
                p.closeInventory();
            }
        }
    }

}
